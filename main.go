package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// SLResponse json data structure for realtime sl response data
type SLResponse struct {
	StatusCode    int
	Message       string
	ExecutionTime uint64
	ResponseData  ResponseData
}

// ResponseData Substructure of above
type ResponseData struct {
	LatestUpdate string
	DataAge      int
	Buses        []ModeofTransport
	Metros       []ModeofTransport
	Trains       []ModeofTransport
	Trams        []ModeofTransport
}

// ModeofTransport substructure of above
type ModeofTransport struct {
	TransportMode        string
	LineNumber           string
	Destination          string
	JourneyDirection     int
	GroupOfLine          string
	StopAreaName         string
	StopAreaNumber       int
	StopPointNumber      int
	StopPointDesignation string
	TimeTabledDateTime   string
	ExpectedDateTime     string
	DisplayTime          string
	JourneyNumber        int
	PredictionState      string
}

func main() {
	outputNagios()
}

// Controls what gets sent to stdout
func outputNagios() {
	// Get variables from cli flags
	trafficType, siteID, warningTimeValue, criticalTimeValue, warningPercentValue, criticalPercentValue := cliFlags()
	// Get parsed json for a given trafficetype
	modeofTransport := getRealtimeDepartures(siteID, trafficType)
	// Calculate delays and percentage of departures that are delayed
	percentDiff, warningPercentDiff, criticalPercentDiff, timeDelay := calculateTimeDiff(modeofTransport, warningTimeValue, criticalTimeValue, warningPercentValue, criticalPercentValue)

	// Some standard phrases
	nagiosOkMsg := "SERVICE STATUS: OK | "
	nagiosWarningMsg := "SERVICE STATUS: WARNING | "
	nagiosCriticalMsg := "SERVICE STATUS: CRITICAL | "

	// OK nagios status, sends performance data for the delays but doesn't warn
	if percentDiff < float64(warningPercentValue) && percentDiff >= 0 {
		min, max := minMax(timeDelay)
		fmt.Printf("%v SiteID: %v Traffictype %v | %v=%v%%;[@]%v:%v;%v;%v\n", nagiosOkMsg, siteID, trafficType, siteID, percentDiff, warningPercentValue, criticalPercentValue, min, max)
		os.Exit(0)
	}
	// WARNING nagios status, sends perf data and also warns
	if warningPercentDiff >= float64(warningPercentValue) && percentDiff < float64(criticalPercentValue) {
		min, max := minMax(timeDelay)
		fmt.Printf("%v SiteID: %v Traffictype %v | %v=%v%%;[@]%v:%v;%v;%v\n", nagiosWarningMsg, siteID, trafficType, siteID, percentDiff, warningPercentValue, criticalPercentValue, min, max)
		os.Exit(1)
	}
	// CRITICAL nagios status, sends perf data and warns critically
	if criticalPercentDiff >= float64(criticalPercentValue) {
		min, max := minMax(timeDelay)
		fmt.Printf("%v SiteID: %v Traffictype %v | %v=%v%%;[@]%v:%v;%v;%v\n", nagiosCriticalMsg, siteID, trafficType, siteID, percentDiff, warningPercentValue, criticalPercentValue, min, max)
		os.Exit(2)
	}
}

// A SO function for getting min max values for perf data
func minMax(array []time.Duration) (time.Duration, time.Duration) {
	var max time.Duration
	var min time.Duration
	if len(array) < 2 {
	} else {
		var max time.Duration = array[0]
		var min time.Duration = array[0]
		for _, value := range array {
			if max < value {
				max = value
			}
			if min > value {
				min = value
			}
		}
		return min, max
	}
	return min, max
}

func calculateTimeDiff(modeofTransport []ModeofTransport, warningTimeValue int, criticalTimeValue int, warningPercentValue int, criticalPercentValue int) (float64, float64, float64, []time.Duration) {

	// Layout of RFC3339 time from trafiklab JSON API
	const layout = "2006-01-02T15:04:05"

	// Delayed departures counter
	var o int
	var w int
	var c int
	// Counter for all departures
	var allDepartures int
	// Initializing slices for delayed departures
	var timeDelay []time.Duration

	// Loop through all departures of a given modeofTransport
	for i := 0; i < len(modeofTransport); i++ {
		// Parse the RFC3339 time from JSON
		timeTableDateTime, err := time.Parse(layout, modeofTransport[i].TimeTabledDateTime)
		if err != nil {
			fmt.Println(err)
		}
		expecteddatetime, err := time.Parse(layout, modeofTransport[i].ExpectedDateTime)
		if err != nil {
			fmt.Println(err)
		}
		// Copy/Convert warningTimeValue into usable time.Duration type
		warningTimeThreshold, _ := time.ParseDuration(strconv.Itoa(warningTimeValue) + "m")
		criticalTimeThreshold, _ := time.ParseDuration(strconv.Itoa(criticalTimeValue) + "m")
		// Calculate time difference and add to slice if difference is not 0
		timeDiff := expecteddatetime.Sub(timeTableDateTime)
		a := timeDiff
		timeDelay = append(timeDelay, a)
		if timeDiff.Minutes() > warningTimeThreshold.Minutes() && timeDiff.Minutes() < criticalTimeThreshold.Minutes() {
			o++
		}
		warningTimeDiff := timeDiff
		if warningTimeDiff.Minutes() >= warningTimeThreshold.Minutes() && timeDiff.Minutes() < criticalTimeThreshold.Minutes() {
			w++
		}
		criticalTimeDiff := timeDiff
		if criticalTimeDiff.Minutes() > criticalTimeThreshold.Minutes() {
			c++
		}
		allDepartures = i
	}
	percentDiff := percentCalc(o, allDepartures)
	warningPercentDiff := percentCalc(w, allDepartures)
	criticalPercentDiff := percentCalc(c, allDepartures)
	return percentDiff, warningPercentDiff, criticalPercentDiff, timeDelay

}

// API call and json unmarshaling into data structure
func getRealtimeDepartures(siteid string, traffictype string) []ModeofTransport {
	// Http GET request
	apiurl := "https://api.sl.se/api2/realtimedeparturesV4.json?key=62884a08ac894507bd74b6e747c427fb&timewindow=60&ship=false&siteid=" + siteid
	resp, err := http.Get(apiurl)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Open body for reading and unmarshal json data into SLResponse struct
	data, _ := ioutil.ReadAll(resp.Body)
	slResponse := &SLResponse{}
	json.Unmarshal([]byte(data), &slResponse)

	// Handle error from trafiklab api
	if slResponse.StatusCode != 0 {
		if slResponse.StatusCode == 4001 {
			fmt.Println("Error: SiteID not recognized or cannot convert to integer")
			os.Exit(3)
		}
		fmt.Println("Unknown error code" + string(slResponse.StatusCode))
	}

	// Return only for one mode of transport
	var modeofTransport []ModeofTransport
	if traffictype == "buses" {
		modeofTransport = slResponse.ResponseData.Buses
	}
	if traffictype == "trains" {
		modeofTransport = slResponse.ResponseData.Trains
	}
	if traffictype == "metros" {
		modeofTransport = slResponse.ResponseData.Metros
	}
	if traffictype == "trams" {
		modeofTransport = slResponse.ResponseData.Trams
	}
	return modeofTransport
}

// Makes code prettier
func percentCalc(a, b int) (percentDiff float64) {
	if b > 0 {
		percentDiff = float64((a * 100) / b)
	} else {
		percentDiff = 0
	}
	return
}

// Get flags from cli and sanity check them
func cliFlags() (trafficType string, siteID string, warningTimeValue int, criticalTimeValue int, warningPercentValue int, criticalPercentValue int) {
	// Get user input of flags
	flag.StringVar(&trafficType, "t", "", "Traffictype, options are: Buses, Trains or Metros")
	flag.StringVar(&siteID, "s", "", "SiteID: ID of desired stop, eg. 9192 for Slussen")
	flag.IntVar(&warningTimeValue, "x", -1, "Warning delay in minutes, all departures above this are counted towards the warning percentage threshold")
	flag.IntVar(&criticalTimeValue, "X", -1, "Critical delay in minutes, all departures above this are counted towards the warning percentage threshold")
	flag.IntVar(&warningPercentValue, "y", -1, "Warning for percentage of departures delayed over -x")
	flag.IntVar(&criticalPercentValue, "Y", -1, "Critical for percentage of departures delayed over -X")
	// Allocate variables
	flag.Parse()

	// Program breaks if these vars are not ONLY lowercase
	trafficType = strings.ToLower(trafficType)
	siteID = strings.ToLower(siteID)

	//Check that variables have been assigned, prints help message
	if trafficType == "" {
		fmt.Println("Traffictype is required")
		flag.PrintDefaults()
		os.Exit(3)
	}
	if siteID == "" {
		fmt.Println("SiteID is required")
		flag.PrintDefaults()
		os.Exit(3)
	}
	if warningTimeValue == -1 {
		fmt.Println("x, Warning delayed time (minutes) is required")
		flag.PrintDefaults()
		os.Exit(3)
	}
	if criticalTimeValue == -1 {
		fmt.Println("X, Critical delayed time (minutes) is required")
		flag.PrintDefaults()
		os.Exit(3)
	}
	if warningPercentValue == -1 {
		fmt.Println("y, Warning percentage is required")
		flag.PrintDefaults()
		os.Exit(3)
	}
	if criticalPercentValue == -1 {
		fmt.Println("Y, Critical percentage is required")
		flag.PrintDefaults()
		os.Exit(3)
	}
	return
}
