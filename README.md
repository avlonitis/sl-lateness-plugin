## SL-lateness Nagios plugin ##

This plugin allows you to monitor your favorite SL stop(s) for delays and to spot trends of increasing lateness with the excellent performance data provided.

## Compiling from source ##

**Prerequisites:** <br>
- The GoLang compiler<br>
&nbsp;&nbsp;&nbsp;&nbsp;- Get it for your system [here](https://golang.org/dl/)

Navigate to wherever your $GOPATH/src is. Usually in your home folder, a directory called "go" <br>
Clone this repo `git clone https://gitlab.com/avlonitis/sl-lateness-plugin` into src directory in your $GOPATH<br>
Inside the freshly cloned directory run `go build -o check_sl_lateness`<br>

If you are on a non-linux machine and plan to run the check on linux, make sure you compile with `GOOS=linux` and `GOARCH=amd64` as environment variables

Now you have an executable check_sl_lateness that you can copy to any machine and configure a nagios service with NRPE.

## Usage ##

Example: `/path/to/check_sl_lateness -x 3 -X 5 -y 10 -Y 20 -t buses -s 9192`

**Full list of options:**<br>
  -x <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Warning delay in minutes, all departures above this are counted towards the warning percentage threshold<br>
  -X <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Critical delay in minutes, all departures above this are counted towards the warning percentage threshold<br>
  -y <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Warning for percentage of departures delayed over -x<br>
  -Y <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Warning for percentage of departures delayed over -X<br>
  -s<br> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SiteID: ID of desired stop, eg. 9192 for Slussen<br>
  -t<br> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Traffictype, options are: Buses, Trains or Metros<br>
        
Get quick help with the `--help` flag `./check_sl_lateness --help`

